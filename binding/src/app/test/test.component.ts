import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template: 'welcome {{name}}<h2>{{greetUser()}}</h2>',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
public name="lili";
  constructor() { }

  ngOnInit(): void {
  }
  greetUser(){
    return "hello"+ this.name;
  }

}
