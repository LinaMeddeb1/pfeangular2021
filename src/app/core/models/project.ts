import {Technologie} from './technologie';
import {Ticket} from './ticket';
import {SubTicket} from './sub-ticket';
import {User} from './auth.models';

export class Project {
  id: number;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  status: string;
  technologies: Array<Technologie>;
  ticket: Array<Ticket>;
  subTicket: Array<SubTicket>;
  createdBy: User;
  progress?: number;
  ticketNumb?: number;
  ticketDone?: number;
}

