import {RoleEnum} from '../enumerations/role.enum';
import {Project} from "./project";

export class User {
    id?: number;
    username?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    email?: string;
    role?: RoleEnum;
    address?: string;
    phone?: string;
    enabled?: boolean;
  profileImageFileName?: string;

    createdProjects?: Array<Project>;
}
