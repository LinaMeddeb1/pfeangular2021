import {Project} from "./project";
import {SubTicket} from "./sub-ticket";
import {User} from "./auth.models";

export class Ticket {
  id: number;
  title: string;
  description: string;
  state: string;
  hoursRequired: number;
  // tslint:disable-next-line:variable-name
  created_at: string;
  project: Project;
  subTicket: Array<SubTicket>;
  parent: Ticket;
  children: Array<Ticket>;
  affectedTo?: User;
  startDate: string;
  endDate: string;
  done: boolean;
  count?: number;
  countSubDone?: number;
  estimation: string;
}
