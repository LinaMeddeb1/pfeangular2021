import {Ticket} from "./ticket";
import {SubTicket} from "./sub-ticket";
import {List} from "../../pages/other/profile/profile.model";

export class TicketTask {
  ticket: Ticket;
  subTicket: Array<SubTicket>;
}
