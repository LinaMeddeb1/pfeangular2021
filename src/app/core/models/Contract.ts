import {User} from "./auth.models";

export class Contract {
  id: number;
  startDate: string;
  endDate: string;
  signedIn: string;
  employee: User;
  type: ContractType;
  enumeration: number;
}
export enum ContractType {
SIVP= 'SIVP',
  CDD= 'CDD',
  CDI= 'CDI',
  KARAMA= 'KARAMA'

}
