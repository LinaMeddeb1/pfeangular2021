import {User} from "./auth.models";

export class OffDay {
  id: number;
  startDAy: string;
  endDate: string;
  employee: User;
  Contract: string;
}
