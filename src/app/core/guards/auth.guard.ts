import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import {UserProfileService} from '../services/user.service';
import {CookieService} from '../services/cookie.service';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userService: UserProfileService,
        private cookieService: CookieService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = JSON.parse(this.cookieService.getCookie('currentUser'));
      // tslint:disable-next-line:triple-equals
        if( route.queryParams['token'] != '' && state.url.indexOf('token') != -1 ) {
          this.userService.confirmAccount(route.queryParams['token']).subscribe(value => {
            console.log('value' + value['sub']);
            this.router.navigate(['apps/users/activate']);
          });
          return false ;
        }
        if (currentUser) {
          // check if route is restricted by role
          if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
            // role not authorised so redirect to home page
            this.router.navigate(['/']);
            return false;
          }

          // logged in so return true
          return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/account/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
