import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/auth.service';
import {CookieService} from '../services/cookie.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,
                private cookieService: CookieService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        // const currentUser = this.authenticationService.currentUser();
        const token = this.cookieService.getCookie('token');
        // alert('hello');
        if (token) {
            request = request.clone({
                setHeaders: {
                  JWTData: `Bearer ${token}`,
                  from: 'api'
                }
            });
        }

        return next.handle(request);
    }
}
