
import { AuthenticationService } from '../services/auth.service';
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {UserProfileService} from "../services/user.service";
@Injectable({
  providedIn: 'root'
})
export class GetImage  {
  constructor(private authenticationService: AuthenticationService, private userService: UserProfileService) { }
  public imageToShow: any ;


  fetch(id: number) {
      this.userService.fetchProfileThumbnail(id.toString())
        .subscribe(image => {  this.createImage(image)},
          err => this.handleImageRetrievalError(err),
        );
  }
  private handleImageRetrievalError(err: Error) {
    console.error(err);
  }

  private createImage(image: Blob): any {
    if (image && image.size > 0) {
      let reader = new FileReader();

      reader.addEventListener("load", () => {
        this.imageToShow = reader.result;
        console.log(this.imageToShow);

      }, false);

      reader.readAsDataURL(image);
    } else {
    }
    return this.imageToShow;

  }
}
