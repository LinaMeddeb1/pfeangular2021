import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {OffDay} from "../models/offDay";
import {Contract} from "../models/Contract";
import {Table} from "../../pages/ui/tables/advanced/advanced.model";
import {ContractTable} from "../../pages/apps/contract/list-contract/contractDataTable";

@Injectable({
  providedIn: 'root'
})
export class ContratServiceService {
  private contractUrl: string;

  constructor(private http: HttpClient) {
    this.contractUrl = 'http://localhost:8080/api/contract';
  }

  public findAll() {
    return this.http.get <Observable<Table[]>>(this.contractUrl);
  }


  getContract(id: number): Observable<Contract> {
    return this.http.get<Contract>(`${this.contractUrl}/${id}`);
  }


  // tslint:disable-next-line:no-shadowed-variable
  public addContract(c: Contract): Observable<any> {
    return this.http.post(`${this.contractUrl}` + '/new', c);

  }
}
