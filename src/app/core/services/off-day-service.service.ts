import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Project} from "../models/project";
import {Technologie} from "../models/technologie";
import {User} from "../models/auth.models";
import {boolean} from "smooth-scrollbar/decorators";
import {Ticket} from "../models/ticket";
import {OffDay} from "../models/offDay";

@Injectable({
  providedIn: 'root'
})
export class OffDayServiceService {
  private offUrl: string;

  constructor(private http: HttpClient) {
    this.offUrl = 'http://localhost:8080/api/offDays';
  }

  public findAll(): Observable<OffDay[]> {
    return this.http.get<OffDay[]>(this.offUrl);
  }


  getOffDay(id: number): Observable<OffDay> {
    return this.http.get<OffDay>(`${this.offUrl}/${id}`);
  }


  // tslint:disable-next-line:no-shadowed-variable
  public addOffday(off: OffDay): Observable<any> {
    return this.http.post(`${this.offUrl}` + '/new', off);

  }

}
