import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Technologie} from '../models/technologie';
import {Project} from "../models/project";

@Injectable({
  providedIn: 'root'
})
export class TechnologieServiceService {
  private technologieUrl: string;

  constructor(private http: HttpClient) {
    this.technologieUrl = 'http://localhost:8080/api/technologies';
  }

  public findAll(): Observable<Technologie[]> {
    return this.http.get<Technologie[]>(this.technologieUrl);
  }

  getProject(id: number): Observable<Technologie> {
    return this.http.get<Technologie>(`${this.technologieUrl}/${id}`);
  }
}
