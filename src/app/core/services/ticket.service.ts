import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Ticket} from "../models/ticket";
import {SubTicket} from "../models/sub-ticket";
import {Project} from "../models/project";
import {TicketTask} from "../models/ticket-task";
import {EventInput} from "@fullcalendar/core";
import {User} from "../models/auth.models";

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  private ticketUrl: string;
  private subTicketUrl: string;
  private eventUrl: string;


  constructor(private http: HttpClient) {
    this.ticketUrl = 'http://localhost:8080/api/tickets';
    this.subTicketUrl = 'http://localhost:8080/api/Tasks';
    this.eventUrl = 'http://localhost:8080/api/events'
  }

  public addTicket(ticket: Ticket, id: number): Observable<any> {
    return this.http.post(`${this.ticketUrl}/new/${id}`, ticket);
  }

  public getEvents() {
    return this.http.get<EventInput[]>(this.eventUrl);
  }

  public updateEvent(id: number, event: EventInput) {
    return this.http.put(`${this.eventUrl}/${id}`, event);
  }

  public reafect(id: number, user: User) {
    return this.http.put(`${this.ticketUrl}/reaffect/${id}`, user);
  }

  public findAllTasks(): Observable<SubTicket[]> {
    return this.http.get<SubTicket[]>(this.subTicketUrl);
  }
  getTicket(id: number): Observable<Ticket> {
    return this.http.get<Ticket>(`${this.ticketUrl}/${id}`);
  }

  getEvent(id: number): Observable<EventInput> {
    return this.http.get<EventInput>(`${this.eventUrl}/${id}`);
  }

  getDepassing(id: number) {
    return this.http.get<boolean>(`${this.ticketUrl}/conflicts/${id}`);
  }

  CompleteTicket(id: number, ticket: Ticket) {
    return this.http.put<Ticket>(`${this.ticketUrl}/${id}/completed`, ticket);
  }
  updateStatus(state: string, ticket: Ticket, id: number) {
    return this.http.put<Ticket>(`${this.ticketUrl}/${id}/update/${state}`, ticket);
  }


  getTicketByState(state: string): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.ticketUrl}/state/${state}`);
  }

  getTicketByUser(id: number): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.ticketUrl}/user/${id}`);
  }
  getAllTickets(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.ticketUrl}/all`);
  }

  getUserResolve(id: number): Observable<User[]> {
    return this.http.get<User[]>(`${this.ticketUrl}/resolve/${id}`);
  }
}
