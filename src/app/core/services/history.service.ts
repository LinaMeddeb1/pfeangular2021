import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {History} from "../models/History";

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  historyUrl : string;

  constructor(private http: HttpClient) {
    this.historyUrl = 'http://localhost:8080/api/history';
  }

  public addHistory(history: History): Observable<History> {
    return this.http.post<History>(`${this.historyUrl}/addHistory`, history);
  }
  public editHistory(history: History, username: string): Observable<History> {
    return this.http.put<History>(`${this.historyUrl}/editHistory/${username}`, history);
  }
  public getHistories(): Observable<History[]> {
    return this.http.get<History[]>(`${this.historyUrl}/getHistories`);
  }
  public findHistoryBySore(id: number): Observable<History> {
    return this.http.get<History>(`${this.historyUrl}findHistoryBySore/${id}`);
  }
  public findHistoryByUsername(username: string): Observable<History> {
    return this.http.get<History>(`${this.historyUrl}/findHistoryByUsername/${username}`);
  }
}
