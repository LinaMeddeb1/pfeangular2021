import { TestBed } from '@angular/core/testing';

import { TechnologieServiceService } from './technologie-service.service';

describe('TechnologieServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TechnologieServiceService = TestBed.get(TechnologieServiceService);
    expect(service).toBeTruthy();
  });
});
