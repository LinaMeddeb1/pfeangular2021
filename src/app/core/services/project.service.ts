import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Project} from '../models/project';
import {catchError, retry} from "rxjs/operators";
import {Technologie} from '../models/technologie';
import {User} from "../models/auth.models";
import {Ticket} from "../models/ticket";
import {boolean} from "smooth-scrollbar/decorators";

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectUrl: string;

  constructor(private http: HttpClient) {
    this.projectUrl = 'http://localhost:8080/api/projects';
  }

  public findAll(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectUrl);
  }

  public searchProject(title: string, description: string): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.projectUrl}/s?search=name==${title};description==${description}`);

  }

  public findTechs(): Observable<Technologie[]> {
    return this.http.get<Technologie[]>(`${this.projectUrl}` + '/tech');
  }

  getUsersByProject(id: number): Observable<User[]> {
    return this.http.get<User[]>(`${this.projectUrl}/pr/${id}`);
  }
  getProjectsByUser(id: number): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.projectUrl}/user/${id}`);
  }

  getProjectsByUserAndStatus(id: number, status: number): Observable<Project[]> {
    return this.http.get<Project[]>(`${this.projectUrl}/user/${id}/${status}`);
  }

  getProject(id: number): Observable<Project> {
    return this.http.get<Project>(`${this.projectUrl}/${id}`);
  }

  countTechPerProject(id: number): Observable<number> {
    return this.http.get<number>(`${this.projectUrl}/${id}/count`);
  }


  getProjectsTickets(id: number, all: boolean): Observable<Ticket[]> {
    const params = new HttpParams()
      .set('id', String(id))
      .set('all', String(all));
    return this.http.get<Ticket[]>(`${this.projectUrl}/project`, {params});
  }

  // tslint:disable-next-line:no-shadowed-variable
  public addProject(Project: Project): Observable<any> {
    return this.http.post(`${this.projectUrl}` + '/new', Project);
    // .pipe(
    // retry(1),
    // catchError(this.handleError)

  }




}
