import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Level} from "../models/Level";

@Injectable({
  providedIn: 'root'
})
export class LevelService {
  levelurl: string;
  constructor(private http: HttpClient) {
    this.levelurl = 'http://localhost:8080/api/level';
  }

  public addLevel(level: Level, idTheme: number): Observable<Level> {
    return this.http.post<Level>(`${this.levelurl}/addLevel/${idTheme}`, level);
  }
  public getLevels(idTheme: number): Observable<Level[]> {
    return this.http.get<Level[]>(`${this.levelurl}/getLevels/${idTheme}`);
  }
  public getLevel(id: number): Observable<Level> {
    return this.http.get<Level>(`${this.levelurl}/getLevel/${id}`);
  }
}
