import { TestBed } from '@angular/core/testing';

import { ContratServiceService } from './contrat-service.service';

describe('ContratServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContratServiceService = TestBed.get(ContratServiceService);
    expect(service).toBeTruthy();
  });
});
