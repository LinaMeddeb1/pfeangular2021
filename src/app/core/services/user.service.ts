import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/auth.models';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Table} from '../../pages/ui/tables/advanced/advanced.model';
import {RoleEnum} from "../enumerations/role.enum";
import {Technologie} from "../models/technologie";
import {Ticket} from "../models/ticket";

@Injectable({ providedIn: 'root' })
export class UserProfileService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Observable<Table[]>>(environment.BASE_END_POINT + 'user/all');
    }
  getByRole(role: RoleEnum): Observable<User[]> {
    return this.http.get<User[]>(environment.BASE_END_POINT + 'user/all/' + role);
  }

  getAccountInfo(email: string) {
      return this.http.get<User>(environment.BASE_END_POINT + 'user/role/' + email);
  }
  createUser(user: User) {
    return this.http.post<User>(environment.BASE_END_POINT + 'user/create', user );
  }
  confirmAccount(token: string) {
    return this.http.post<string>(environment.BASE_END_POINT + 'user/activate?token=' + token, token );
  }
  updateAccount(user: User) {
  return this.http.put<User>(environment.BASE_END_POINT + 'user/update', user );
}


  fetchProfileImage(userId: string): Observable<Blob> {
    const url = 'http://localhost:8080/api/user/' + userId + '/profileImage';
    console.log('Profile image URL is ' + url);

    return this.http.get(url, { responseType: 'blob' });
  }
  fetchProfileThumbnail(userId: string): Observable<Blob> {
    const url = 'http://localhost:8080/api/user/' + userId + '/profileThumbnail';
    console.log('Profile image URL is ' + url);

    return this.http.get(url, { responseType: 'blob' });
  }

}
