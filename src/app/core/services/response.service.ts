import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Response} from "../models/Response";

@Injectable({
  providedIn: 'root'
})
export class ResponseService {
  responseUrl: string;
  constructor(private http: HttpClient) {
    this.responseUrl = 'http://localhost:8080/api/questions';
  }

  public addResponse(response: Response, idQuestion: number): Observable<Response> {
    return this.http.post<Response>(`${this.responseUrl}/addResponse/${idQuestion}`, response);
  }
  public getResponses(idQuestion: number): Observable<Response[]> {
    return this.http.get<Response[]>(`${this.responseUrl}/getResponses/${idQuestion}`);
  }
}
