import { TestBed } from '@angular/core/testing';

import { OffDayServiceService } from './off-day-service.service';

describe('OffDayServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OffDayServiceService = TestBed.get(OffDayServiceService);
    expect(service).toBeTruthy();
  });
});
