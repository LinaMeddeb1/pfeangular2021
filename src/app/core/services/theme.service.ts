import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Theme} from "../models/Theme";


@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  themeUrl: string;

  constructor(private http: HttpClient) {
    this.themeUrl = 'http://localhost:8080/api/theme';
  }

  public addTheme(theme: Theme): Observable<Theme> {
    return this.http.post<Theme>(`${this.themeUrl}/addTheme`, theme);
  }
  public getThemes(): Observable<Theme[]> {
    return this.http.get<Theme[]>(`${this.themeUrl}/getThemes`);
  }
  public getTheme(id: number): Observable<Theme> {
    return this.http.get<Theme>(`${this.themeUrl}/getTheme/${id}`);
  }
}
