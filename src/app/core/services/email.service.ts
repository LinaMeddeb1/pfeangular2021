import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Email} from "../models/email";
import {Observable} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};


@Injectable({    providedIn: 'root'
})
export class EmailService {
 emailUrl : string;
  constructor(private http: HttpClient) {
    this.emailUrl = 'http://localhost:8080/api';

  }

  fetchInbox(): Observable<Email[]> {
    let url = `${this.emailUrl}/email/inbox`;
    console.log("Fetch inbox URL is " + url);

    return this.http.get<Email[]>(url);
  }

  getDisplayableFrom(from: string): string {
    let cleanText: string = from.replace(/<\/?[^>]+(>|$)/g, "");

    if (cleanText.length > 20) {
      cleanText = cleanText.substring(0, 21) + '...';
    }

    return cleanText;
  }
}
