import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Question} from "../models/Question";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  questionUrl: string;
  constructor(private http: HttpClient) {
    this.questionUrl='http://localhost:8080/api/question';
  }

  public addQuestion(question: Question, idLevel: number): Observable<Question> {
    return this.http.post<Question>(`${this.questionUrl}/addQuestion/${idLevel}`, question);
  }
  public getQuestions(idLevel: number): Observable<Question[]> {
    return this.http.get<Question[]>(`${this.questionUrl}/getQuestions/${idLevel}`);
  }
}
