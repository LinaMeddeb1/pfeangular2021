import {Component, OnInit, AfterViewInit, Injectable} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../../core/services/auth.service';
import { UserProfileService } from '../../../core/services/user.service';
import {HttpClient} from '@angular/common/http';
import {CookieService} from '../../../core/services/cookie.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  error = '';
  loading = false;
  public errorMessage = '';

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
              private authenticationService: AuthenticationService,
              private userService: UserProfileService,
              private cookieService: CookieService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    this.errorMessage = '';
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .subscribe(
          this.handleResponseToken,
          this.handleError
        );
  }
  handleResponseToken = token => {
    this.cookieService.deleteCookie('token');
    if (token && token.id_token) {
      this.cookieService.setCookie('token', token.id_token, 1);
      this.userService
        .getAccountInfo(this.f.email.value)
        .subscribe(this.handleResponseUser, this.handleError);
    } else {
      this.errorMessage = 'Email et mot de passe non valides';
    }
  }
  handleResponseUser = account => {
    this.cookieService.setCookie('currentUser', JSON.stringify(account), 1);
    this.router.navigate([this.returnUrl]);
    this.loading = false;
  }
  handleError = err => {
    if (err && err.error && err.error.apierror) {
      this.errorMessage = err.error.apierror.message;
      this.loading = false;
    } else {
      throw err;
    }
  }
}

