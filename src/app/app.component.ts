import { Component } from '@angular/core';
import {User} from "./core/models/auth.models";
import {AuthenticationService} from "./core/services/auth.service";
import {Router} from "@angular/router";
import {RoleEnum} from "./core/enumerations/role.enum";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.currentUser = this.authenticationService.currentUser();
  }
  get isAdmin() {
    return this.currentUser && this.currentUser.role === RoleEnum.ADMINISTRATOR;
  }


}
