import { Component, OnInit } from '@angular/core';
import {Theme} from "../../../../core/models/Theme";
import {ThemeService} from "../../../../core/services/theme.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-list-theme',
  templateUrl: './list-theme.component.html',
  styleUrls: ['./list-theme.component.scss']
})
export class ListThemeComponent implements OnInit {
  themes: Theme[];
  userUsername: string;
  constructor(private themeService: ThemeService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userUsername = this.route.snapshot.params.userUsername;
    this.themeService.getThemes().subscribe(themes => {
      this.themes = themes;
    });
  }

}
