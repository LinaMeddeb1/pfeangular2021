import { Component, OnInit } from '@angular/core';
import {Level} from "../../../../core/models/Level";
import {LevelService} from "../../../../core/services/level.service";
import {ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-content-theme',
  templateUrl: './content-theme.component.html',
  styleUrls: ['./content-theme.component.scss']
})
export class ContentThemeComponent implements OnInit {
  level: any;
  levels: Level[];
  id: number;
  idLevel: number;
  userName: string;

  constructor(private levelService: LevelService, private route: ActivatedRoute, private userService: AuthenticationService) {
    this.route.params.subscribe(
      params => {
        this.userName = userService.currentUser().firstName;
        this.id = this.route.snapshot.params.id;
        this.levelService.getLevels(this.id).subscribe(data => {
          this.levels = data;
        });
      }
    );
  }

  ngOnInit(): void {
  }

  setLevel(e) {
    this.idLevel = e;
    console.log(this.idLevel);
  }

}
