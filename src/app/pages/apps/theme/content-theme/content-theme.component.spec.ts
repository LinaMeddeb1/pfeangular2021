import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentThemeComponent } from './content-theme.component';

describe('ContentThemeComponent', () => {
  let component: ContentThemeComponent;
  let fixture: ComponentFixture<ContentThemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentThemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentThemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
