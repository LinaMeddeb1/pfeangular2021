import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListThemeComponent} from "./list-theme/list-theme.component";
import {ContentThemeComponent} from "./content-theme/content-theme.component";
import {QuizComponent} from "./quiz/quiz.component";


const routes: Routes = [
  {path: 'theme', component: ListThemeComponent,
    children: [
      {path: 'quizz/:idLevel', component: QuizComponent},
      {path: 'themeContent/:id', component: ContentThemeComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule { }
