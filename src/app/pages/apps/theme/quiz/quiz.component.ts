import { Component, OnInit } from '@angular/core';
import {Question} from "../../../../core/models/Question";
import {MatDialog} from "@angular/material/dialog";
import {QuestionService} from "../../../../core/services/question.service";
import {ActivatedRoute} from "@angular/router";
import {HistoryService} from "../../../../core/services/history.service";
import {LevelService} from "../../../../core/services/level.service";
import {AuthenticationService} from "../../../../core/services/auth.service";
import {timer} from "rxjs";
import {History} from "../../../../core/models/History";
import {ResultComponent} from "../result/result.component";
const counterSecond = timer(0, 1000);


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  dialogRef: any;
  questions: Question[];
  history: History = {} as History;
  histories: History[];
  idLevel: number;
  level: any;
  counter = 0;
  score = 0;
  btnDisabled = true;
  username: string;

  constructor(private dialog: MatDialog, private questionService: QuestionService, private route: ActivatedRoute,
              private levelService: LevelService, private historyService: HistoryService,
              private userService: AuthenticationService) {}

  ngOnInit(): void {
    this.username = this.userService.currentUser().firstName;
    this.idLevel = this.route.snapshot.params.idLevel;
    this.questionService.getQuestions(this.idLevel).subscribe(data => {
      this.questions = data;
      for (var i = this.questions.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = this.questions[i];
        this.questions[i] = this.questions[j];
        this.questions[j] = temp;
      }
    });
    this.levelService.getLevel(this.idLevel).subscribe(level => {
      this.level = level;
    });
    counterSecond.subscribe(() => {
      this.counter += 1;
    });
  }


  public openResult(themeName, levelName) {
    const counter = this.counter;
    const score = this.score;
    const username = this.username;
    this.history.total = this.counter;
    this.history.themeName = themeName;
    this.history.levelName = levelName;
    this.history.username = username;
    this.history.sore = this.score;
    console.log(this.history);
    console.log(score);
    this.dialogRef = this.dialog.open(ResultComponent, {
      width: '420px',
      data: {score, counter, themeName, levelName}
    });
    this.historyService.findHistoryByUsername(username).subscribe(data => {
      this.history = data;
      console.log('init score' + score);
      if (score > this.history.sore) {
        this.history.sore = score;
        this.historyService.addHistory(this.history).subscribe(addHistory => {
          this.history = addHistory;
        });

      } else {
        this.historyService.editHistory(this.history, username).subscribe((editHistory) => {
          this.history = editHistory;
        });
      }
    });

    counterSecond.subscribe(() => {
      this.counter = +'';
    });
  }

  setResponses(e) {
    this.questions.forEach((q, index) => {
      if (e.source.value === q.correct) {
        this.score++;
      }
    });
    this.btnDisabled = false;
  }
}
