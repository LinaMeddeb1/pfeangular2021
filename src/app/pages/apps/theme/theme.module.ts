import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemeRoutingModule } from './theme-routing.module';
import { ListThemeComponent } from './list-theme/list-theme.component';
import { AddThemeComponent } from './add-theme/add-theme.component';
import {FormsModule} from "@angular/forms";
import { ContentThemeComponent } from './content-theme/content-theme.component';
import {MatRadioModule} from "@angular/material/radio";
import { QuizComponent } from './quiz/quiz.component';
import { ResultComponent } from './result/result.component';


@NgModule({
  declarations: [ListThemeComponent, AddThemeComponent, ContentThemeComponent, QuizComponent, ResultComponent],
  imports: [
    CommonModule,
    ThemeRoutingModule,
    FormsModule,
    MatRadioModule
  ],
  exports: [ AddThemeComponent , ResultComponent],
  entryComponents: [ResultComponent]

})
export class ThemeModule { }
