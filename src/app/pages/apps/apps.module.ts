import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { UIModule } from '../../shared/ui/ui.module';
import { AppsRoutingModule } from './apps-routing.module';

import { EmailModule } from './email/email.module';
import { ProjectModule } from './project/project.module';
import { TasksModule } from './tasks/tasks.module';
import { FullCalendarModule } from '@fullcalendar/angular';

import { CalendarComponent } from './calendar/calendar.component';
import {UsersModule} from './users/users.module';
import {FieldErrorDisplayComponent} from "./field-error-display/field-error-display.component";
import {TicketModule} from "./Ticket/ticket.module";
import {TechnologiesModule} from "./Technologies/technologies.module";
import {TasksprModule} from "./TasksPr/taskspr.module";
import { SearchComponent } from './search/search.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FlatpickrModule} from "angularx-flatpickr";
import {MatSelectModule} from "@angular/material/select";
import { CalendarPComponent } from './calendar-p/calendar-p.component';
import {ContractModule} from "./contract/contract.module";
import { InboxComponent } from './inbox/inbox.component';
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {QuestionModule} from "./question/question.module";
import {LevelModule} from "./level/level.module";
import {ThemeModule} from "./theme/theme.module";

@NgModule({
    declarations: [CalendarComponent, SearchComponent, CalendarPComponent, InboxComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    FullCalendarModule,
    AppsRoutingModule,
    UIModule,
    EmailModule,
    ProjectModule,
    TasksModule,
    TicketModule,
    TechnologiesModule,
    TasksprModule,
    UsersModule,
    QuestionModule,
    LevelModule,
    ThemeModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FlatpickrModule,
    MatSelectModule,
    ContractModule,
    MatTableModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCheckboxModule
  ]
})

export class AppsModule { }
