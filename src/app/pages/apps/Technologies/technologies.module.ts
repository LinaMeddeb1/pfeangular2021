import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TechnologiesRoutingModule } from './technologies-routing.module';
import {TechListComponent} from "./tech-list/tech-list.component";


@NgModule({
  declarations: [TechListComponent],
  imports: [
    CommonModule,
    TechnologiesRoutingModule
  ]
})
export class TechnologiesModule { }
