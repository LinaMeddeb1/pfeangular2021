import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {Technologie} from '../../../../core/models/technologie';
import {TechnologieServiceService} from '../../../../core/services/technologie-service.service';

@Component({
  selector: 'app-tech-list',
  templateUrl: './tech-list.component.html',
  styleUrls: ['./tech-list.component.scss']
})
export class TechListComponent implements OnInit {

  projects: Observable<Technologie[]>;
  constructor(private router: Router, private technologieservice: TechnologieServiceService) { }

  ngOnInit() {
    this.reloadData();
  }


  private reloadData() {

    this.projects  = this.technologieservice.findAll();
  }
}
