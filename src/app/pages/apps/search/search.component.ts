import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjectService} from "../../../core/services/project.service";
import {Project} from "../../../core/models/project";
import {TechnologieServiceService} from "../../../core/services/technologie-service.service";
import {Technologie} from "../../../core/models/technologie";
import {Observable} from "rxjs";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  title: string;
  description: string;
   technologies: Observable<Technologie[]>;
  status: Array<string> = ['finiched', 'ongoing'];

  constructor(private projectService: ProjectService,
              private formBuilder: FormBuilder,
              private techservice: TechnologieServiceService) { }

  ngOnInit() {
    this.technologies = this.techservice.findAll();

    this.searchForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  save() {
    this.title = this.searchForm.value.name;
    this.description = this.searchForm.value.description;
    this.projectService.searchProject(this.title, this.description).subscribe(data => { console.log(data);
        },
      error => console.log(error),
    );

  }

}
