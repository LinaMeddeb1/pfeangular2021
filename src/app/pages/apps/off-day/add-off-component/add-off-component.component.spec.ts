import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOffComponentComponent } from './add-off-component.component';

describe('AddOffComponentComponent', () => {
  let component: AddOffComponentComponent;
  let fixture: ComponentFixture<AddOffComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOffComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOffComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
