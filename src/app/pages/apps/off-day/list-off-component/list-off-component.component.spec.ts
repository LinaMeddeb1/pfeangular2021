import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOffComponentComponent } from './list-off-component.component';

describe('ListOffComponentComponent', () => {
  let component: ListOffComponentComponent;
  let fixture: ComponentFixture<ListOffComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOffComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOffComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
