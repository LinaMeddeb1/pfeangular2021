import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffDayRoutingModule } from './off-day-routing.module';
import { AddOffComponentComponent } from './add-off-component/add-off-component.component';
import { ListOffComponentComponent } from './list-off-component/list-off-component.component';


@NgModule({
  declarations: [AddOffComponentComponent, ListOffComponentComponent],
  imports: [
    CommonModule,
    OffDayRoutingModule
  ]
})
export class OffDayModule { }
