import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AddOffComponentComponent} from "./add-off-component/add-off-component.component";


const routes: Routes = [
  {
    path: 'off-new',
    component: AddOffComponentComponent
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffDayRoutingModule { }
