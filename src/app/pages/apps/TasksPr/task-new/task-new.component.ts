import { Component, OnInit } from '@angular/core';
import {ProjectService} from "../../../../core/services/project.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TicketService} from "../../../../core/services/ticket.service";
import {SubTicket} from "../../../../core/models/sub-ticket";
import {Ticket} from "../../../../core/models/ticket";

@Component({
  selector: 'app-task-new',
  templateUrl: './task-new.component.html',
  styleUrls: ['./task-new.component.scss']
})
export class TaskNewComponent implements OnInit {
  subticket: SubTicket = new SubTicket();
  taskForm: FormGroup;

  constructor(private ticketservice: TicketService,
              private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.subticket = new SubTicket();
    this.taskForm = this.formBuilder.group({
      task: ['', [Validators.required]]

    });
  }
  save() {
    this.subticket = this.taskForm.value;
  }


}
