import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksprRoutingModule } from './taskspr-routing.module';
import {TaskNewComponent} from "./task-new/task-new.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [TaskNewComponent],
  imports: [
    CommonModule,
    TasksprRoutingModule,
    ReactiveFormsModule
  ]
})
export class TasksprModule { }
