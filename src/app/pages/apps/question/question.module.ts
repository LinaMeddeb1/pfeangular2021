import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionRoutingModule } from './question-routing.module';
import { QuestionComponent } from './question/question.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import {MatRadioModule} from "@angular/material/radio";
import {FormsModule} from "@angular/forms";
import { ContentQuestionComponent } from './content-question/content-question.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {ThemeModule} from "../theme/theme.module";
import {AddThemeComponent} from "../theme/add-theme/add-theme.component";
import {ListLevelComponent} from "../level/list-level/list-level.component";
import { ResponsesComponent } from './responses/responses.component';
import {AddLevelComponent} from "../level/add-level/add-level.component";


@NgModule({
  declarations: [QuestionComponent, AddQuestionComponent, ContentQuestionComponent, ResponsesComponent],
  imports: [
    CommonModule,
    QuestionRoutingModule,
    MatRadioModule,
    FormsModule,
    MatExpansionModule,
    ThemeModule,
],
  exports: [ContentQuestionComponent, AddQuestionComponent],
  entryComponents: [AddThemeComponent, ListLevelComponent , AddLevelComponent],

})
export class QuestionModule { }
