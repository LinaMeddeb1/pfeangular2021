import { Component, OnInit } from '@angular/core';
import {Theme} from "../../../../core/models/Theme";
import {MatDialog} from "@angular/material/dialog";
import {ThemeService} from "../../../../core/services/theme.service";
import {AddThemeComponent} from "../../theme/add-theme/add-theme.component";
import {AddLevelComponent} from "../../level/add-level/add-level.component";

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  dialogRef: any;
  themes: Theme[];

  constructor(private dialog: MatDialog, private themeService: ThemeService) {}

  ngOnInit(): void {
    this.themeService.getThemes().subscribe(themes => {
      this.themes = themes;
    });
  }

  public openRegister() {
    this.dialogRef = this.dialog.open(AddThemeComponent, {
      width: '420px'
    });
  }

  addLevel(id: number) {
    this.dialogRef = this.dialog.open(AddLevelComponent, {
      width: '420px',
      data: {id}
    });
  }

}
