import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuestionService} from "../../../core/services/question.service";
import {QuestionComponent} from "./question/question.component";
import {ListLevelComponent} from "../level/list-level/list-level.component";
import {ContentQuestionComponent} from "./content-question/content-question.component";
import {ProjectListComponent} from "../project/project-list/project-list.component";
import {Content} from "@angular/compiler/src/render3/r3_ast";


const routes: Routes = [
  {
    path: 'question',
    component: QuestionComponent ,
    children: [
      {path: 'level/:id', component: ListLevelComponent,
        children: [
          {path: 'questionContent/:id', component: ContentQuestionComponent}
        ]}

    ] },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionRoutingModule { }
