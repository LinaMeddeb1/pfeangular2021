import { Component, OnInit } from '@angular/core';
import {Question} from "../../../../core/models/Question";
import {Response} from "../../../../core/models/Response";

import {MatDialog} from "@angular/material/dialog";
import {ResponseService} from "../../../../core/services/response.service";
import {QuestionService} from "../../../../core/services/question.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-content-question',
  templateUrl: './content-question.component.html',
  styleUrls: ['./content-question.component.scss']
})
export class ContentQuestionComponent implements OnInit {
  questions: Question[];
  responses: Response[];
  dialogRef: any;
  id: number;

  constructor(private dialog: MatDialog, private questionService: QuestionService, private route: ActivatedRoute,
              private responseService: ResponseService) {
    this.route.params.subscribe(
      params => {
        this.id = this.route.snapshot.params.id;
        this.questionService.getQuestions(this.id).subscribe(questions => {
          this.questions = questions;
        });
      }
    );
  }

  ngOnInit(): void {
  }
  // tslint:disable-next-line: typedef

  findResponse(id: number) {

  }
}
