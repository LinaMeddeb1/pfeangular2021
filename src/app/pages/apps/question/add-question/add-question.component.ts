import {Component, Inject, OnInit} from '@angular/core';
import {Question} from "../../../../core/models/Question";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {QuestionService} from "../../../../core/services/question.service";
import {ResponseService} from "../../../../core/services/response.service";

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {
  question: Question = {} as Question;
  correct: any = {};

  constructor(private dialog: MatDialog, private questionService: QuestionService,
              private responseService: ResponseService, @Inject(MAT_DIALOG_DATA) private data: any) { }

  ngOnInit(): void {
  }

  cancel() {
    this.dialog.closeAll();
  }

  setResponse() {
    this.question.correct = this.correct;
  }

  addQuestion() {
    this.questionService.addQuestion(this.question, this.data.id).subscribe(question => {
      this.question = question;
      window.location.reload();
    })
  }

}
