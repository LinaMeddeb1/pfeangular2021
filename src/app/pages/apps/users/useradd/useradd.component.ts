import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserProfileService} from '../../../../core/services/user.service';
import {CookieService} from '../../../../core/services/cookie.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-useradd',
  templateUrl: './useradd.component.html',
  styleUrls: ['./useradd.component.scss']
})
export class UseraddComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
              private userService: UserProfileService, private cookieService: CookieService) { }

  ngOnInit() {

  }

}
