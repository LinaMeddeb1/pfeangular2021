import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {MustMatch} from '../../../ui/form/validation/validation.mustmatch';
import {RoleEnum} from '../../../../core/enumerations/role.enum';
import {UserProfileService} from '../../../../core/services/user.service';
import {User} from '../../../../core/models/auth.models';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {

  signupForm: FormGroup;
  submitted = false;
  error = '';
  loading = false;
  selectValue: string[];
  // tslint:disable-next-line:ban-types
  user: User;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
              private userService: UserProfileService) { }

  ngOnInit() {

    this.signupForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: [''], //
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      phoneNumber: [''], //
 //
      role: ['', Validators.required], //
    });
    this.selectValue = [RoleEnum.ADMINISTRATOR, RoleEnum.CLIENT, RoleEnum.DEVELOPER, RoleEnum.MANAGER ,
      RoleEnum.RH , RoleEnum.INTERN ];
  }

  ngAfterViewInit() {
    document.body.classList.add('authentication-bg');
    document.body.classList.add('authentication-bg-pattern');
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;
    console.log('here');
    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    }

    this.loading = true;

    this.user = {email: this.f.email.value, firstName: this.f.firstname.value,
      lastName: this.f.lastname.value, role: this.f.role.value, enabled: false, username: this.f.username.value };
    console.log(JSON.stringify(this.user));

    this.userService.createUser(this.user).subscribe(
    value => {alert('Utilisateur ajouté \n token générer=' + value); },
      (er) => console.log(er.getMessages())
      );

    // setTimeout(() => {
    //   this.loading = false;
    //   this.router.navigate(['/account/confirm']);
    // }, 1000);
  }
}
