import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserProfileService} from '../../../../core/services/user.service';
import {CookieService} from '../../../../core/services/cookie.service';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss']
})
export class ActivateComponent implements OnInit {

  token: string;
  constructor(private route: ActivatedRoute, private router: Router,
              private userService: UserProfileService, private cookieService: CookieService) { }

  ngOnInit() {
    let user = JSON.parse(this.cookieService.getCookie('currentUser'));
    console.log(this.route.queryParamMap.subscribe(paramMap => {
      this.token = paramMap.get('token');
    }));
    this.userService.confirmAccount(this.token).subscribe(value => {
      console.log('cest bon' + value);
      alert('Bienvenue, votre compte a été confirmé');
      this.router.navigate(['apps/users/profile']);
    });
  }

}
