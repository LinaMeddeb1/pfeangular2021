import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthenticationService} from "../../../../core/services/auth.service";
import {ImageService} from "../../../../core/services/image.service";
import {UploadFileService} from "../../../../core/services/upload-file.service";
import {UploadedImage} from "../../../../core/models/uploaded-image";
import {HttpEvent, HttpResponse} from "@angular/common/http";
import {UserProfileService} from "../../../../core/services/user.service";
import {User} from "../../../../core/models/auth.models";
import {CookieService} from "../../../../core/services/cookie.service";

const profileImageUploadUrl = 'http://localhost:8080/api/user/profileImage';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class ProfileImageComponent implements OnInit {

  currentFileUpload: UploadedImage;
  changeImage: boolean = false;
  uploading: boolean = false;
  imageToShow: any = null;
  user: User;
  showSpinner: boolean = true;
  private profile: any;

  constructor(private uploadService: UploadFileService, private userService: UserProfileService,
              private imageService: ImageService, private cookieService: CookieService) { }

  ngOnInit() {
    this.profile = JSON.parse(this.cookieService.getCookie('currentUser'));

    this.userService.fetchProfileImage(this.profile.id)
      .subscribe(image => this.createImage(image),
        err => this.handleImageRetrievalError(err));
  }

  private handleImageRetrievalError(err: Error) {
    console.error(err);
    this.showSpinner = false;
  }

  private createImage(image: Blob) {
    if (image && image.size > 0) {
      let reader = new FileReader();

      reader.addEventListener('load', () => {
        this.imageToShow = reader.result;
        this.showSpinner = false;
      }, false);

      reader.readAsDataURL(image);
    } else {
      this.showSpinner = false;
    }
  }

  change($event) {
    this.changeImage = true;
  }

  upload() {
    this.uploading = true;

    this.uploadService.pushFileToStorage(this.currentFileUpload.file, profileImageUploadUrl)
      .subscribe(event => this.handleEvent(event),
        err => this.handleError(err));
    location.reload();

  }

  handleEvent(event: HttpEvent<any>) {
    if (event instanceof HttpResponse) {
      let response: HttpResponse<any> = <HttpResponse<any>>event;
      if (response.status === 200) {
        this.handleGoodResponse();
      }
    }
  }

  handleGoodResponse() {
    this.currentFileUpload = undefined;
    this.uploading = false;
    this.displaySuccess();
  }

  handleError(err: Error) {
    console.error("Error is", err);
    this.uploading = false;
    this.displayError(err.message);
  }

  onUploadedImage(image: UploadedImage) {
    let imageError: string = this.imageService.validateImage(image);

    if (!imageError) {
      this.currentFileUpload = image;
    } else {
      this.displayError(imageError);
    }

  }

  private displayError(message: string) {

  }

  private displaySuccess() {

  }
}
