import {AdvancedService} from '../../../ui/tables/advanced/advanced.service';
import {SearchResult , Table} from './UserDataTable';
import {Injectable, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {Observable, of} from 'rxjs';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

/**
 * interface pour déclarer les colonnes de notre entité
 */

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

/**
 * Sort the table data
 * @param tables Table field value
 * @param column Fetch the column
 * @param direction Sort direction Ascending or Descending
 */
function sort(tables: Table[], column: string, direction: string): Table[] {
  if (direction === '') {
    return tables;
  } else {
    return [...tables].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

/**
 * Table Data Match with Search input
 * @param tables Table field value fetch
 * @param term Search the value
 */
function matches(tables: Table, term: string, pipe: PipeTransform) {
  return (isNotNullOrUndefined(tables.id) && pipe.transform(tables.id).includes(term))
    || (isNotNullOrUndefined(tables.username) && tables.username.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.firstName) && tables.firstName.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.lastName) && tables.lastName.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.email) && tables.email.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.role) && tables.role.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.phone) && tables.phone.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.address) && tables.address.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.enabled) && tables.enabled.toString().includes(term))
    || (isNotNullOrUndefined(tables.privileges) && tables.privileges.toString().toLowerCase().includes(term));
}

@Injectable({
  providedIn: 'root'
})
export class UserDataTableService extends AdvancedService {

  constructor(pipe: DecimalPipe) {
    super(pipe);
  }
  public _search(): Observable<SearchResult> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

    // 1. sort
    let tables = sort(this.tableData, sortColumn, sortDirection);
if (tables !== undefined){
    // 2. filter
    tables = tables.filter(table => matches(table, searchTerm, this.pipe));
    const total = tables.length;

    // 3. paginate
    this.totalRecords = tables.length;
    this._state.startIndex = (page - 1) * this.pageSize;
    this._state.endIndex = (page - 1) * this.pageSize + this.pageSize;
    if (this.endIndex > this.totalRecords) {
      this.endIndex = this.totalRecords;
    }
    tables = tables.slice(this._state.startIndex, this._state.endIndex);

    return of(
      { tables, total }
    );
  }}
}
