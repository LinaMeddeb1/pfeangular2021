import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {UserProfileService} from '../../../../core/services/user.service';
import {Table} from '../../../ui/tables/advanced/advanced.model';
import {Observable} from 'rxjs';
import {AdvancedSortableDirective, SortEvent} from '../../../ui/tables/advanced/advanced-sortable.directive';
import {DecimalPipe, KeyValue} from '@angular/common';
import {UserDataTableService} from './userDataTableService';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss'],
  providers: [UserDataTableService, DecimalPipe] // declarer dataTable providers ici
})
export class UserlistComponent implements OnInit {
   p: Observable<Table[]> = new Observable<[]>();
  constructor(public service: UserDataTableService, private userService: UserProfileService) {
    // tslint:disable-next-line: max-line-length
    this.total$ = this.service.total$;
    console.log(this.total$);
    this.userService.getAll().subscribe(
      value => { this.p = value;
        console.log(this.p);



      },
      error => {},
      () => { this.keys = Object.keys(this.p[0]);
        while (this.excludedKeys.length > 0) {
          // tslint:disable-next-line:prefer-const
          let elementToRemove = this.excludedKeys.pop();
          this.keys.splice(this.keys.indexOf(elementToRemove), 1);

        }
        console.log(this.keys);
        if (this.p !== undefined){
          this.service.tableData = this.p;
          this.tables$ = this.service.tables$;

        }
      }
    );


  }
  // bread crum data
  breadCrumbItems: Array<{}>;
  // Table keys
  keys: any;
  excludedKeys = ['createdAt', 'createdBy', 'updatedBy','token','profileImageFileName', 'updatedAt', 'password'];

  tables$: Observable<Table[]>;
  total$: Observable<number>;

  @ViewChildren(AdvancedSortableDirective) headers: QueryList<AdvancedSortableDirective>;


  ngOnInit() {

    this.breadCrumbItems = [{ label: 'Shreyu', path: '/' }, { label: 'Tables', path: '/' }, { label: 'Advanced', path: '/', active: true }];
  }

  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

}
