// Interface qui comporte les colonnes qu'on veut afficher
export interface Table {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  role: string;
  phone: string;
  address: string;
  enabled: string;
  privileges: string;
}
// Search Data
export interface SearchResult {
  tables: Table[];
  total: number;
}
