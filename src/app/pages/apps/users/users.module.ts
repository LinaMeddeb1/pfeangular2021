import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UseraddComponent } from './useradd/useradd.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  NgbAlertModule,
  NgbPaginationModule,
  NgbProgressbarModule,
  NgbTabsetModule,
  NgbTooltipModule, NgbTypeaheadModule
} from '@ng-bootstrap/ng-bootstrap';
import {SignupComponent} from './signup/signup.component';
import {UIModule} from '../../../shared/ui/ui.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { UsereditComponent } from './useredit/useredit.component';
import { UserlistComponent } from './userlist/userlist.component';
import {TablesModule} from '../../ui/tables/tables.module';
import { ActivateComponent } from './activate/activate.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { ProfileImageComponent } from './profile-image/profile-image.component';
import {MatButtonModule} from "@angular/material/button";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { AddContractComponent } from './add-contract/add-contract.component';


@NgModule({
  declarations: [UseraddComponent, SignupComponent, UsereditComponent, UserlistComponent, ActivateComponent, ImageUploaderComponent, ProfileImageComponent, AddContractComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
    NgbAlertModule,
    NgbTabsetModule,
    NgbTooltipModule,
    NgbProgressbarModule,
    UIModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    TablesModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ]
})
export class UsersModule { }
