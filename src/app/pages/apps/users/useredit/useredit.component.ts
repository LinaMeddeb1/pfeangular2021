import { Component, OnInit } from '@angular/core';
import { messageData, activities, tasks, projectData } from './data';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Message, Activity, Tasks, List } from './profile.model';
import {MustMatch} from '../../../ui/form/validation/validation.mustmatch';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../../core/models/auth.models';
import {UserProfileService} from '../../../../core/services/user.service';
import {CookieService} from '../../../../core/services/cookie.service';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-useredit',
  templateUrl: './useredit.component.html',
  styleUrls: ['./useredit.component.scss']
})
export class UsereditComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  messageData: Message[];
  activities: Activity[];
  tasks: Tasks[];
  projectData: List[];

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  editForm: FormGroup;
  submitted = false;
  error = '';
  profile: any;

  user: User;
  imageToShow: any = null;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router,
              private userService: UserProfileService, private cookieService: CookieService, private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.breadCrumbItems = [{label: 'Shreyu', path: '/'}, {label: 'Pages', path: '/'}, {
      label: 'Profile',
      active: true
    }];


    this.editForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: [''], //
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      phoneNumber: [''], //
      address: [''],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confirmpwd: ['', Validators.required], //
      // role: ['', Validators.required], //
    });
    this.profile = JSON.parse(this.cookieService.getCookie('currentUser'));
    this.userService.getAccountInfo(this.profile.email)
      .subscribe(data => {
        console.log(data);

        this.user = data;
      }, error => console.log(error));
    this.userService.fetchProfileImage(this.profile.id)
      .subscribe(image => this.createImage(image),
        err => this.handleImageRetrievalError(err));

    this.editForm = this.formBuilder.group({
      firstname: this.profile.firstName,
      lastname: this.profile.lastName,
      username: this.profile.username,
      email: this.profile.email,
      phoneNumber: this.profile.phoneNumber,
      address: this.profile.address,
      password: this.profile.password,
      confirmpwd: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmpwd'),
    });

    console.log(this.profile);
    this._fetchData();
  }

  /**
   * Get form
   */
  get f() {
    return this.editForm.controls;
  }

  /**
   * Fetches the data
   */
  private _fetchData() {
    this.messageData = messageData;
    this.activities = activities;
    this.tasks = tasks;
    this.projectData = projectData;
  }
  private handleImageRetrievalError(err: Error) {
    console.error(err);
  }

  private createImage(image: Blob) {
    if (image && image.size > 0) {
      let reader = new FileReader();

      reader.addEventListener("load", () => {
        this.imageToShow = reader.result;
      }, false);

      reader.readAsDataURL(image);
    } else {
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.user = {
      email: this.f.email.value,
      firstName: this.f.firstname.value,
      lastName: this.f.lastname.value,
      password: this.f.password.value,
      username: this.f.username.value,
      phone: this.f.phoneNumber.value,
      address: this.f.address.value,
    };
    console.log(JSON.stringify(this.user));

    this.userService.updateAccount(this.user).subscribe(
      value => {
        alert('' + value);
      },
      (er) => console.log(er.getMessages())
    );
    this.cookieService.deleteCookie('currentUser');
    location.reload();



    // tslint:disable-next-line:max-line-length
    // this.profile = {id: this.profile.id, token: this.profile.token, email : this.f.email.value, password : this.f.password.value, firstName: this.f.firstname.value,
    //   lastName: this.f.lastname.value, role: this.f.role.value, enabled : false};

    // setTimeout(() => {
    //   this.loading = false;
    //   this.router.navigate(['/account/confirm']);
    // }, 1000);
  }

  // Gets called when the user selects an image
  public onFileChanged(event) {
    // Select File
    this.selectedFile = event.target.files[0];
  }
}
