import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UseraddComponent} from './useradd/useradd.component';
import {UsereditComponent} from './useredit/useredit.component';
import {UserlistComponent} from './userlist/userlist.component';
import {ActivateComponent} from './activate/activate.component';
import {AuthGuard} from "../../../core/guards/auth.guard";
import {RoleEnum} from "../../../core/enumerations/role.enum";
import {ProfileImageComponent} from "./profile-image/profile-image.component";


const routes: Routes = [
  {
    path: 'users/new',
    component: UseraddComponent,
    canActivate: [AuthGuard], data: { roles: [RoleEnum.ADMINISTRATOR ] }
  },
  {
    path: 'users/profile',
    component: UsereditComponent
  },
  {
    path: 'users/list',
    component: UserlistComponent,
    canActivate: [AuthGuard], data: { roles: [RoleEnum.ADMINISTRATOR ]}
    },
  {
    path: 'users/activate',
    component: ActivateComponent
  },
  {
    path: 'users/profile-image',
    component: ProfileImageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
