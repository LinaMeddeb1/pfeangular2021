import { Component, OnInit } from '@angular/core';
import {Email} from "../../../core/models/email";
import {MatTableDataSource} from "@angular/material/table";
import {EmailService} from "../../../core/services/email.service";

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  displayedColumns: string[] = ['action', 'from', 'subject'];
  private loading: boolean = true;
  dataSource: MatTableDataSource<Email>;


  constructor(private emailService: EmailService) { }

  ngOnInit(): void {
    this.loadInbox();
  }

  private loadInbox() {
    this.emailService.fetchInbox().subscribe(
      (emails: Email[]) => this.handleInboxRetrieval(emails),
      (err: Error) => this.handleInboxError(err)
    );
  }

  private handleInboxRetrieval(emails: Email[]) {
    console.log(emails);
    this.dataSource = new MatTableDataSource(emails);
    this.loading = false;
  }

  private handleInboxError(err: Error) {
    console.error(err);
    // this.alertService.error("Problem loading emails!");
    this.loading = false;
  }
}
