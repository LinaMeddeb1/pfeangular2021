import { Component, OnInit } from '@angular/core';
import {AddQuestionComponent} from "../../question/add-question/add-question.component";
import {Level} from "../../../../core/models/Level";
import {LevelService} from "../../../../core/services/level.service";
import {ActivatedRoute} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-list-level',
  templateUrl: './list-level.component.html',
  styleUrls: ['./list-level.component.scss']
})
export class ListLevelComponent implements OnInit {
  levels: Level[];
  idTheme: number;
  dialogRef: any;

  constructor(private levelService: LevelService,
              private route: ActivatedRoute, private dialog: MatDialog) {
    this.route.params.subscribe(
      params => {
        this.idTheme = this.route.snapshot.params.id;
        this.levelService.getLevels(this.idTheme).subscribe(levels => {
          this.levels = levels;
        });
      }
    );
  }

  ngOnInit(): void {

  }
  public openRegister(id: number) {
    this.dialogRef = this.dialog.open(AddQuestionComponent, {
      width: '500px',
      data: {id}
    });
  }

}
