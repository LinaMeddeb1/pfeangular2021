import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListLevelComponent} from "./list-level/list-level.component";
import {ContentQuestionComponent} from "../question/content-question/content-question.component";


const routes: Routes = [ {path: 'level/:id', component: ListLevelComponent,
  children: [
    {path: 'questionContent/:id', component: ContentQuestionComponent}
  ]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LevelRoutingModule { }
