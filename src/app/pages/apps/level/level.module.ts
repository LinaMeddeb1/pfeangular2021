import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LevelRoutingModule } from './level-routing.module';
import { ListLevelComponent } from './list-level/list-level.component';
import { AddLevelComponent } from './add-level/add-level.component';
import {FormsModule} from "@angular/forms";
import {AddQuestionComponent} from "../question/add-question/add-question.component";


@NgModule({
  declarations: [ListLevelComponent, AddLevelComponent],
    imports: [
        CommonModule,
        LevelRoutingModule,
        FormsModule
    ],
  exports: [ListLevelComponent, AddLevelComponent],
  entryComponents: [AddQuestionComponent]
})
export class LevelModule { }
