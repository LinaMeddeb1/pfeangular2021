import {Component, Inject, OnInit} from '@angular/core';
import {Level} from "../../../../core/models/Level";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {LevelService} from "../../../../core/services/level.service";

@Component({
  selector: 'app-add-level',
  templateUrl: './add-level.component.html',
  styleUrls: ['./add-level.component.scss']
})
export class AddLevelComponent implements OnInit {
  level: Level = {} as Level;

  constructor(public dialogRef: MatDialogRef<AddLevelComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any, private levelService: LevelService) { }

  ngOnInit(): void {

  }

  addLevel() {
    this.levelService.addLevel(this.level, this.data.id).subscribe(level => {
      this.level = level;
      console.log(this.level);
    });
  }

}
