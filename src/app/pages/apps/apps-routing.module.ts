import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalendarComponent } from './calendar/calendar.component';
import {SearchComponent} from "./search/search.component";
import {InboxComponent} from "./inbox/inbox.component";

const routes: Routes = [
    {
        path: 'apps-calendar',
        component: CalendarComponent
    },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'inbox',
    component: InboxComponent
  },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppsRoutingModule { }
