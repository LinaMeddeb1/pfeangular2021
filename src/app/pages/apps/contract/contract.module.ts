import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractRoutingModule } from './contract-routing.module';
import { AddCOntractComponent } from './add-contract/add-contract.component';
import { ListContractComponent } from './list-contract/list-contract.component';
import { DetailsContractComponent } from './details-contract/details-contract.component';
import {MatStepperModule} from "@angular/material/stepper";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {NgbModule, NgbPaginationModule} from "@ng-bootstrap/ng-bootstrap";
import {TablesModule} from "../../ui/tables/tables.module";
import {UIModule} from "../../../shared/ui/ui.module";


@NgModule({
  declarations: [AddCOntractComponent, ListContractComponent, DetailsContractComponent],
  imports: [
    CommonModule,
    ContractRoutingModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    NgbPaginationModule,
    NgbModule,
    TablesModule,
    FormsModule,
    UIModule
  ]
})
export class ContractModule { }
