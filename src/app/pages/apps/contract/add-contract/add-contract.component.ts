import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Ticket} from "../../../../core/models/ticket";
import {User} from "../../../../core/models/auth.models";
import {TicketService} from "../../../../core/services/ticket.service";
import {ProjectService} from "../../../../core/services/project.service";
import {UserProfileService} from "../../../../core/services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleEnum} from "../../../../core/enumerations/role.enum";
import {Contract} from "../../../../core/models/Contract";
import {ContratServiceService} from "../../../../core/services/contrat-service.service";

@Component({
  selector: 'app-add-contract',
  templateUrl: './add-contract.component.html',
  styleUrls: ['./add-contract.component.scss']
})
export class AddCOntractComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  contract: Contract;
  idCont: number;
  isLinear = false;
  min: Date;
  users: User[];



  constructor(public contractService: ContratServiceService,
              private formBuilder: FormBuilder,
              public userProfileService: UserProfileService,
              public actRoute: ActivatedRoute,
              public router: Router
  ) {}


  ngOnInit() {
    this.contract = new Contract();
    this.min = new Date();

    // recuprerate id from path
    this.idCont = this.actRoute.snapshot.params.id;
    this.getUserByRole();

    this.firstFormGroup = this.formBuilder .group({type: ['', [Validators.required]],
      startDate: new FormControl(),
      endDate: new FormControl(),
      signatureDate: new FormControl(),
      children: this.formBuilder.array([])});



  }
  getUserByRole(){
    return this.userProfileService.getByRole(RoleEnum.DEVELOPER)
      .subscribe(users => {
        this.users = users;
      });
  }

  get children(): FormArray {
    return this.firstFormGroup.get('children') as FormArray;
  }

  get diagnostic() {
    return JSON.stringify(this.firstFormGroup.value);
  }


  submit() {
    this.contract = this.firstFormGroup.value;
    this.contractService.addContract(this.contract).subscribe(data => console.log(data),
      error => console.log(error));
    this.contract = new Contract();
    this.router.navigate(['contract-list']);

  }
}
