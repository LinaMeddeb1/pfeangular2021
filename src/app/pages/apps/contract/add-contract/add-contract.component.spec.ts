import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCOntractComponent } from './add-contract.component';

describe('AddCOntractComponent', () => {
  let component: AddCOntractComponent;
  let fixture: ComponentFixture<AddCOntractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCOntractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCOntractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
