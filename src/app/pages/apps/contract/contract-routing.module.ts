import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddCOntractComponent} from "./add-contract/add-contract.component";
import {ListContractComponent} from "./list-contract/list-contract.component";
import {DetailsContractComponent} from "./details-contract/details-contract.component";


const routes: Routes = [
  {
    path: 'new',
    component: AddCOntractComponent
  },
  {
    path: 'list',
    component: ListContractComponent
  },
{
  path: 'details',
    component: DetailsContractComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractRoutingModule { }
