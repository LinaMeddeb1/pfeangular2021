import {User} from "../../../../core/models/auth.models";

export interface ContractTable {
  id: number;
  startDate: string;
  endDate: string;
  signatureDate: string;
  employee: User;
}
// Search Data
export interface SearchResultContract {
  tables: ContractTable[];
  total: number;
}
