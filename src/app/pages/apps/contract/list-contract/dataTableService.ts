
import {Injectable, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {Observable, of} from 'rxjs';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';
import {AdvancedService} from '../../../ui/tables/advanced/advanced.service';
import {ContractTable, SearchResultContract} from './contractDataTable';

/**
 * interface pour déclarer les colonnes de notre entité
 */

function compare(v1, v2) {
  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

/**
 * Sort the table data
 * @param tables Table field value
 * @param column Fetch the column
 * @param direction Sort direction Ascending or Descending
 */
function sort(tables: ContractTable[], column: string, direction: string): ContractTable[] {
  if (direction === '') {
    return tables;
  } else {
    return [...tables].sort((a, b) => {
      const res = compare(a[column], b[column]);
      return direction === 'asc' ? res : -res;
    });
  }
}

/**
 * Table Data Match with Search input
 * @param tables Table field value fetch
 * @param term Search the value
 * @param pipe
 */
function matches(tables: ContractTable, term: string, pipe: PipeTransform) {
  return (isNotNullOrUndefined(tables.startDate) && tables.startDate.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.endDate) && tables.endDate.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.employee.username) && tables.employee.username.toLowerCase().includes(term))
    || (isNotNullOrUndefined(tables.signatureDate) && tables.signatureDate.toLowerCase().includes(term))
   ;
}

@Injectable({
  providedIn: 'root'
})
export class DataTableService extends AdvancedService {

  constructor(pipe: DecimalPipe) {
    super(pipe);
  }
  public _search(): Observable<SearchResultContract> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

    // 1. sort
    let tables = sort(this.tableData, sortColumn, sortDirection);

    // 2. filter
    tables = tables.filter(table => matches(table, searchTerm, this.pipe));
    const total = tables.length;

    // 3. paginate
    this.totalRecords = tables.length;
    this._state.startIndex = (page - 1) * this.pageSize;
    this._state.endIndex = (page - 1) * this.pageSize + this.pageSize;
    if (this.endIndex > this.totalRecords) {
      this.endIndex = this.totalRecords;
    }
    tables = tables.slice(this._state.startIndex, this._state.endIndex);

    return of(
      { tables, total }
    );
  }
}
