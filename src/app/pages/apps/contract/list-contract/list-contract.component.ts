import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {User} from '../../../../core/models/auth.models';

import {ActivatedRoute, Router} from '@angular/router';
import {UserProfileService} from '../../../../core/services/user.service';
import {AuthenticationService} from '../../../../core/services/auth.service';
import {ContratServiceService} from '../../../../core/services/contrat-service.service';
import {Contract} from '../../../../core/models/Contract';
import {Observable} from 'rxjs';
import {Table} from '../../../ui/tables/advanced/advanced.model';
import {DecimalPipe} from '@angular/common';
import {AdvancedSortableDirective, SortEvent} from '../../../ui/tables/advanced/advanced-sortable.directive';
import {DataTableService} from './dataTableService';
import {ContractTable} from "./contractDataTable";

@Component({
  selector: 'app-list-contract',
  templateUrl: './list-contract.component.html',
  styleUrls: ['./list-contract.component.scss'],
  providers: [DataTableService, DecimalPipe]
})
export class ListContractComponent implements OnInit {

  id: number;
  contracts: Contract[];
  current: User;
  // bread crum data
  breadCrumbItems: Array<{}>;
  // Table keys
  keys: any;

  tables$: Observable<Table[]>;
  total$: Observable<number>;
  excludedKeys = [];
  @ViewChildren(AdvancedSortableDirective) headers: QueryList<AdvancedSortableDirective>;


  constructor(public contractService: ContratServiceService,
              public service: DataTableService) {
    let p: Observable<Table[]>;
    this.total$ = service.total$;
    this.contractService.findAll().subscribe(
      value => { p = value;
                 console.log(p);

                 this.service.tableData = value;
                 this.tables$ = service.tables$;
      },
      error => {
      console.log(error); }
    );

  }





  ngOnInit() {}


  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }}
