import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin, {Draggable} from '@fullcalendar/interaction';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import listPlugin from '@fullcalendar/list';
import { EventInput } from '@fullcalendar/core';

import { Event } from './event.model';

import { category, calendarEvents } from './data';
import {TicketService} from "../../../core/services/ticket.service";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {esLocale, frLocale} from "ngx-bootstrap/chronos";
import {DatePipe} from "@angular/common";
import {AuthenticationService} from "../../../core/services/auth.service";
import {RoleEnum} from "../../../core/enumerations/role.enum";
import {User} from "../../../core/models/auth.models";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [DatePipe],

})

/**
 * Calendar component - handling calendar with sidebar and content
 */
export class CalendarComponent implements OnInit {
  date: string;
  newRvent: EventInput;

  // bread crumb items
  breadCrumbItems: Array<{}>;

  // create event
  formCreateEvent: FormGroup;

  // edit event
  formEditEvent: FormGroup;

  submitted: boolean;

  // Form category data
  category: Event[];

  // Date added in event
  newEventDate: Date;

  // Edit event
  editEvent: EventInput;

  // Delete event
  deleteEvent: EventInput;

  // calendar plugin
  calendarPlugins = [dayGridPlugin, bootstrapPlugin, timeGrigPlugin, interactionPlugin, listPlugin];
  calendarWeekends: any;
  // show events
  calendarEvents: EventInput[];

  options: any;
  dragable = false ;
  eventsModel: any;
  @ViewChild('calendar',
    {static: true}) fullcalendar: FullCalendarComponent;
  @ViewChild('external', {static: true}) external: ElementRef;
   current: User;
  constructor(private modalService: NgbModal, public datePipe: DatePipe,
              private formBuilder: FormBuilder, private ts: TicketService,
              private auth: AuthenticationService) {
    this.current = this.auth.currentUser();
    if (this.current.role === RoleEnum.ADMINISTRATOR) {
      this.dragable = true;
  } else {this.dragable = false;}}

  ngOnInit() {




    this.breadCrumbItems = [{ label: 'Shreyu', path: '/' }, { label: 'Apps', path: '/' }, { label: 'Calendar', active: true }];

    this.formCreateEvent = this.formBuilder.group({
      name: ['', [Validators.required]],
      category: ['', [Validators.required]],
    });

    /**
     * Edit Event Model Data
     */
    this.formEditEvent = this.formBuilder.group({
      editTitle: ['', [Validators.required]],
    });

    /**
     * Fetches Data
     */
    this._fetchData();
  }


  eventClick(model) {
    console.log(model);
  }
  eventDragStop(model) {
    console.log(model);
  }
  dateClick(model) {
    console.log(model);
  }

  /**
   * Returns the form
   */
  get createForm() {
    return this.formCreateEvent.controls;
  }

  /**
   * Edit form
   */
  get editForm() {
    return this.formEditEvent.controls;
  }

  /**
   * Creates new event
   */
  createNewEvent() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.formCreateEvent.invalid) {
      return;
    }

    if (this.formCreateEvent.valid) {
      const title = this.formCreateEvent.get('name').value;
      // tslint:disable-next-line: no-shadowed-variable
      const category = this.formCreateEvent.get('category').value;

      this.calendarEvents = this.calendarEvents.concat({
        id: this.calendarEvents.length + 1,
        title,
        className: category,
        start: this.newEventDate || new Date()
      });
      this.formCreateEvent = this.formBuilder.group({
        name: '',
        category: ''
      });
      this.modalService.dismissAll();
    }
  }

  /**
   * Open Event Modal
   * @param content modal content
   * @param event calendar event
   */
  openModal(content: any, event?: any) {
    this.newEventDate = event ? event.date : new Date();
    this.modalService.open(content);
  }

  /**
   * Open Event Modal For Edit
   * @param editcontent modal content
   * @param event calendar event
   */
  openEditModal(editcontent: any, event: any) {
    this.formEditEvent = this.formBuilder.group({
      editTitle: event.event.title,
    });
    // tslint:disable-next-line: max-line-length
    this.editEvent = { id: event.event.id, title: event.event.title, start: event.event.start, classNames: event.event.classNames };
    this.modalService.open(editcontent);
  }

  /**
   * Upldated event title save in calendar
   */
  editEventSave() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.formEditEvent.invalid) {
      return;
    }

    const editTitle = this.formEditEvent.get('editTitle').value;
    const editId = this.calendarEvents.findIndex(x => x.id + '' === this.editEvent.id + '');
    // tslint:disable-next-line: radix
    this.calendarEvents[editId] = { ...this.editEvent, title: editTitle, id: parseInt(this.editEvent.id + ''), className: '' };
    this.formEditEvent = this.formBuilder.group({
      editTitle: '',
    });
    this.modalService.dismissAll();
  }

  /**
   * Delete the event from calendar
   */
  deleteEventData() {
    const deleteId = this.editEvent.id;
    const deleteEvent = this.calendarEvents.findIndex(x => x.id + '' === deleteId + '');
    this.calendarEvents[deleteEvent] = { ...this.deleteEvent, id: '' };
    delete this.calendarEvents[deleteEvent].id;
    this.modalService.dismissAll();
  }

  /**
   * Fetches the required data
   */
  private _fetchData() {
    // Event category
    this.category = category;
    // Calender Event Data
    this.ts.getEvents().subscribe(data => this.calendarEvents = data);

    // form submit
    this.submitted = false;
  }


  eventDrop(dropInfo) {
    this.date = this.datePipe.transform(dropInfo.event.start, 'yyyy-MM-dd');
    console.log(dropInfo.event);
    this.ts.getEvent(dropInfo.event.id).subscribe(data => {
      this.newRvent = data;
      console.log(data);
      this.newRvent.start = this.date;
      console.log(this.newRvent);
      this.ts.updateEvent(dropInfo.event.id, this.newRvent).subscribe(data =>
      {console.log(data);});


    });

    console.log(this.date);

  }


}
