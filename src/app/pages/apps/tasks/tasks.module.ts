import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../shared/ui/ui.module';

import {NgbDropdownModule, NgbCollapseModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { TasksRoutingModule } from './tasks-routing.module';

import { DndModule } from 'ngx-drag-drop';

import { TasklistComponent } from './tasklist/tasklist.component';
import { KanbanboardComponent } from './kanbanboard/kanbanboard.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  imports: [
    CommonModule,
    TasksRoutingModule,
    UIModule,
    NgbDropdownModule,
    NgbCollapseModule,
    DndModule,
    NgbAlertModule,
    MatDialogModule,
    MatButtonModule
  ],
    declarations: [TasklistComponent, KanbanboardComponent]
})

export class TasksModule { }
