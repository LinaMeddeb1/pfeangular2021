import {Component, EventEmitter, OnInit} from '@angular/core';

import { tasks } from './data';

import { Tasks } from './tasklist.model';
import {TicketService} from "../../../../core/services/ticket.service";
import {ProjectService} from "../../../../core/services/project.service";
import {AuthenticationService} from "../../../../core/services/auth.service";
import {User} from "../../../../core/models/auth.models";
import {Ticket} from "../../../../core/models/ticket";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.scss'],
  providers: [DatePipe],
})

/**
 * Task-list component - handling task-list with sidebar and content
 */
export class TasklistComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  todayTask: boolean;
  upcomingTasks: boolean;
  otherTasks: boolean;

  todayTasks: Ticket[];
  upcomingTask: Ticket[];
  otherTask: Ticket[];

  tasks: Ticket[];

  current: User;
  all = false;
  showAllMsg = false;
  currentProjectId: number;
  alertClose = false;
  disableListProject = false;
  close: EventEmitter<void>;
   date: string;
  constructor(public ticketservice: TicketService ,
              public projectService: ProjectService ,
              public auth: AuthenticationService,
              public datePipe: DatePipe) {
    this.date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    this.current = this.auth.currentUser();
    if (this.current.role === 'ROLE_ADMINISTRATOR'){
      this.ticketservice.getAllTickets()
        .subscribe(data => {
          this.tasks = data;
          this._fetchData();

        });
    } else if (this.current.role === 'ROLE_DEVELOPER') {
      this.ticketservice.getTicketByUser(this.current.id)
        .subscribe(data => {
          if (data) {
            this.tasks = data;
            this._fetchData();
          } else {
           }

        });

    }
    // tslint:disable-next-line:max-line-length
    this.breadCrumbItems = [{ label: 'Shreyu', path: '/' }, { label: 'Apps', path: '/' }, { label: 'Tasks', path: '/' }, { label: 'Tasks Board', active: true }];


    /**
     * Fetches the task data
     */
  }

  /**
   * Fetches and filter the data
   */
  private _fetchData() {
    this.todayTasks = this.tasks.filter(t => t.endDate === this.date);
    this.upcomingTask = this.tasks.filter(t => t.endDate === 'upcoming');
    this.otherTask = this.tasks.filter(t => t.endDate === 'other');
  }
}
