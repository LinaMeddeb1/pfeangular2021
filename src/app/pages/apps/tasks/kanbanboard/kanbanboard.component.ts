import {Component, EventEmitter, OnInit} from '@angular/core';

import { DndDropEvent } from 'ngx-drag-drop';

import { Task } from './board.model';

import { tasks } from './data';
import {Ticket} from "../../../../core/models/ticket";
import {TicketService} from "../../../../core/services/ticket.service";
import {Observable} from "rxjs";
import {ProjectService} from "../../../../core/services/project.service";
import {Project} from "../../../../core/models/project";
import {User} from "../../../../core/models/auth.models";
import {AuthenticationService} from "../../../../core/services/auth.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";

@Component({
  selector: 'app-kanbanboard',
  templateUrl: './kanbanboard.component.html',
  styleUrls: ['./kanbanboard.component.scss']
})

/**
 * Kanbanboard component - handling task-board with sidebar and content
 */
export class KanbanboardComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  tickets: Ticket[];
  // Task data
  todoTasks: Ticket[];
  inprogressTasks: Ticket[];
  reviewTasks: Ticket[];
  doneTasks: Ticket[];
  ticket: Ticket;
  projects: Project[];
  displayMessage: string = 'Choose a project';
  allMessage = 'View all tickets';
  current: User;
  all = false;
  showAllMsg = false;
  currentProjectId: number;
  alertClose = false;
  disableListProject = false;
  close: EventEmitter<void>;


  constructor(public ticketservice: TicketService ,
              public projectService: ProjectService ,
              public auth: AuthenticationService ) { }

  ngOnInit() {
    // tslint:disable-next-line: max-line-length
    this.current = this.auth.currentUser();
    if (this.current.role === 'ROLE_ADMINISTRATOR'){
    this.projectService.findAll()
      .subscribe(data => {
        this.projects = data;
       });
    } else if (this.current.role === 'ROLE_DEVELOPER') {
      this.projectService.getProjectsByUser(this.current.id)
        .subscribe(data => {
          if (data) {
          this.projects = data;  } else {
            this.alertClose = true;
            this.disableListProject =  true; }

        });

    }
    // tslint:disable-next-line:max-line-length
    this.breadCrumbItems = [{ label: 'Shreyu', path: '/' }, { label: 'Apps', path: '/' }, { label: 'Tasks', path: '/' }, { label: 'Tasks Board', active: true }];
  }


  /**
   * On task drop event
   */
  onDrop(event: DndDropEvent, filteredList?: any[], targetStatus?: string) {
    if (filteredList && event.dropEffect === 'move') {
      let index = event.index;

      if (typeof index === 'undefined') {
        index = filteredList.length;
      }
      console.log(event.data);
      filteredList.splice(index, 0, event.data);
      this.ticket = event.data;
      this.ticketservice.updateStatus(targetStatus, this.ticket, this.ticket.id).subscribe(
        value => {
          alert('' + value);
        },
        (er) => console.log(er.getMessages())
      );
    }

  }


  /**
   * on dragging task
   * @param item item dragged
   * @param list list from item dragged
   */
  onDragged(item: any, list: any[]) {
    const index = list.indexOf(item);
    list.splice(index, 1);
  }

  /**
   * Fetches the value of kanbanboard data
   */
  private _fetchData() {
    // all tasks

    if (this.tickets) {
      this.todoTasks = this.tickets.filter(t => t.state === 'ToDo');
      this.inprogressTasks = this.tickets.filter(t => t.state === 'Doing');
      this.reviewTasks = this.tickets.filter(t => t.state === 'Confirmed');
      this.doneTasks = this.tickets.filter(t => t.state === 'Done');
    }
  }

  changeMsg(name: string, id: number) {
    this.displayMessage = name;
    this.showAllMsg = true ;

    console.log(this.showAllMsg);

    this.projectService.getProjectsTickets(id, this.all)
      .subscribe(data => {
        this.tickets = data;
        /**
         * Fetches Data
         */
        if (data) {
        this._fetchData(); }
        this.currentProjectId = id; });

  }
  changeAllMessage(msg: string) {
    if(msg === 'Show My tickets') {
      this.all = false;
    this.allMessage = 'View all tickets';} else {
      this.all = true;
      this.allMessage =  'Show My tickets';
    }
    this.projectService.getProjectsTickets(this.currentProjectId, this.all)
      .subscribe(data => {

        /**
         * Fetches Data
         */
        if (data) {
          this.tickets = data;
          this._fetchData(); } else {

        this.todoTasks = [];
        this.doneTasks = [];
        this.reviewTasks = [];
        this.inprogressTasks = [];
        console.log(this.tickets);
        }});

  }
}
