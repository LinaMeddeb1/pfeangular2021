import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../../core/services/project.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Technologie} from '../../../../core/models/technologie';
import {TechnologieServiceService} from '../../../../core/services/technologie-service.service';
import {Project} from '../../../../core/models/project';
import {UserProfileService} from "../../../../core/services/user.service";
import {User} from "../../../../core/models/auth.models";
import {RoleEnum} from "../../../../core/enumerations/role.enum";

@Component({
  selector: 'app-project-new',
  templateUrl: './project-new.component.html',
  styleUrls: ['./project-new.component.scss']
})
export class ProjectNewComponent implements OnInit {
  project: Project = new Project();
  technologies: Observable<Technologie[]>;
  technologiesList: Technologie[];
  projectForm: FormGroup;
  submitted = false;
  // tslint:disable-next-line:max-line-length
  min = new Date();
  start = new Date();
  private formSumitAttempt: boolean;

  constructor(private projectService: ProjectService,
              private formBuilder: FormBuilder,
              private userService: UserProfileService,
              private router: Router,
              private techservice: TechnologieServiceService) {
  }

  get diagnostic() {
    return JSON.stringify(this.projectForm.value);
  }

  ngOnInit(): void {
    this.reloadData();

    this.projectForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      technologies: ['', [Validators.required]]
    });

  }
  isFieldValid(field: string) {
    return (
      (!this.projectForm.get(field).valid && this.projectForm.get(field).touched) ||
      (this.projectForm.get(field).untouched && this.formSumitAttempt)
    );
  }


  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  save() {
    this.project = this.projectForm.value;
    this.projectService.addProject(this.project).subscribe(data => { console.log(data);
                                                                     this.project = new Project();
                                                                     this.goToList(); },
        error => console.log(error),
    );

  }

  onSubmit() {
    this.submitted = true;
    this.save();

  }

  private reloadData() {

    this.technologies = this.techservice.findAll();
  }

  private goToList() {
    this.router.navigate(['project-list']);

  }
}
