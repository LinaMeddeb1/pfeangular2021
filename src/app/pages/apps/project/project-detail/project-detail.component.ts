import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectService} from '../../../../core/services/project.service';
import {Project} from '../../../../core/models/project';
import {TicketService} from '../../../../core/services/ticket.service';
import {Ticket} from '../../../../core/models/ticket';
import {User} from "../../../../core/models/auth.models";
import {UserProfileService} from "../../../../core/services/user.service";
import {AuthenticationService} from "../../../../core/services/auth.service";
import {GetImage} from "../../../../core/helpers/getImage";


@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  id: number;
  idT: number;
  project: Project;
  count: [];
  countTech: number;
  items: number[];
  ticket: Ticket;
  user: User;
  users: User[];
  completed: boolean;
  subcompleted: boolean;
  imageToShow: string | ArrayBuffer;
  imageExp: any;
  current: User;
  subCount?: number;


  constructor(public projectService: ProjectService,
              public ticketService: TicketService,
              public actRoute: ActivatedRoute,
              public userService: UserProfileService,
              public auth: AuthenticationService,
              public router: Router,
              private getImage: GetImage) {
  }

  ngOnInit() {
    this.current = this.auth.currentUser();
    console.log(this.current.role);
    this.project = new Project();
    // recuprerate id from path
    this.id = this.actRoute.snapshot.params.id;
    // gett project
    this.projectService.getProject(this.id)
      .subscribe(data => {
        console.log(data);
        this.project = data;
        this.getUsersByProject(this.id);
      }, error => console.log(error));
    if (this.user != null) {
    this.userService.fetchProfileThumbnail(this.user.id.toString())
        .subscribe(image => this.createImage(image),
          err => this.handleImageRetrievalError(err));

  }

  }
  private getUsersByProject(id: number) {
    this.projectService.getUsersByProject(id)
      .subscribe(users => {
        console.log('gggggggg');
        console.log(users);

        this.users = users;
      }, error => console.log(error));
  }

  private handleImageRetrievalError(err: Error) {
    console.error(err);
  }

  private createImage(image: Blob) {
    if (image && image.size > 0) {
      let reader = new FileReader();

      reader.addEventListener("load", () => {
        this.imageToShow = reader.result;
      }, false);

      reader.readAsDataURL(image);
    } else {
    }
  }


  newTicket(id: number) {
    this.router.navigate([id, 'ticket']);
  }


  ticketDetails(i: Ticket) {
   this.idT = i.id;
   this.ticket = new Ticket();
   if (i.countSubDone === i.children.length && i.children.length !== 0) {
     this.subcompleted = true;
   }

   this.ticketService.getTicket(this.idT.valueOf())
      .subscribe(data => {
        console.log('data', data);

        this.ticket = data;
        console.log('this.ticket.affectedTo.id', this.ticket.affectedTo.id);


        this.getImage.fetch(this.ticket.affectedTo.id);
        this.imageExp = this.getImage.imageToShow;
        console.log('this.imageExp', this.imageExp);
      }, error => console.log(error));

  }

}
