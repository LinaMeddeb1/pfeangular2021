import {Component, OnInit} from '@angular/core';
import {Project} from '../../../../core/models/project';
import {ProjectService} from '../../../../core/services/project.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from "../../../../core/services/auth.service";
import {RoleEnum} from "../../../../core/enumerations/role.enum";
import {User} from "../../../../core/models/auth.models";

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  id: number;
  projects: Project[];
   current: User;
  constructor(private router: Router, private projectService: ProjectService,
              private auth: AuthenticationService) { }

  ngOnInit() {
    this.reloadData();
  }


  addProject(): void {
   this.router.navigate(['project-new']);
  }

  projectDetails(id: number) {
   this.router.navigate(['project-detail', id]);
  }
  private reloadData() {
    this.current = this.auth.currentUser();
    if (this.current.role === RoleEnum.DEVELOPER) {
      this.projectService.getProjectsByUser(this.current.id).subscribe(data =>{ this.projects = data;
        console.log(this.projects);
      });
      console.log(this.projects);

    } else {
     this.projectService.findAll().subscribe(data => this.projects = data);
    }
  }


  ongoing() {

    this.projects = this.projects.filter(t => t.status === 'ongoing');
    console.log(this.projects);

  }

  finished() {
    this.projects = this.projects.filter(t => t.status === 'finished');


  }
}
