import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {ProjectListComponent} from "./project-list/project-list.component";
import {ProjectDetailComponent} from "./project-detail/project-detail.component";
import {ProjectNewComponent} from "./project-new/project-new.component";
import {AuthGuard} from "../../../core/guards/auth.guard";
import {RoleEnum} from '../../../core/enumerations/role.enum';

const routes: Routes = [
    {
        path: 'project-list',
        component: ProjectListComponent,
      canActivate: [AuthGuard], data: { roles: [RoleEnum.ADMINISTRATOR , RoleEnum.DEVELOPER]}
      },
  {
    path: 'project-detail/:id',
    component: ProjectDetailComponent,
    canActivate: [AuthGuard], data: { roles: [RoleEnum.ADMINISTRATOR , RoleEnum.DEVELOPER]}

  },
  {
    path: 'project-new',
    component: ProjectNewComponent
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProjectRoutingModule { }
