import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from '../../../shared/ui/ui.module';

import { ProjectRoutingModule } from './project-routing.module';
import { NgbTabsetModule, NgbProgressbarModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectDetailComponent} from "./project-detail/project-detail.component";
import {ProjectNewComponent} from "./project-new/project-new.component";
import {AppComponent} from "../../../app.component";
import {FieldErrorDisplayComponent} from "../field-error-display/field-error-display.component";
import {ReactiveFormsModule} from "@angular/forms";
import {TicketModule} from "../Ticket/ticket.module";


@NgModule({
  imports: [
    CommonModule,
    UIModule,
    ProjectRoutingModule,
    NgbTabsetModule,
    NgbTooltipModule,
    NgbProgressbarModule,
    ReactiveFormsModule,
    TicketModule,
  ],
    declarations: [ProjectListComponent, ProjectDetailComponent, ProjectNewComponent, FieldErrorDisplayComponent],
})

export class ProjectModule { }
