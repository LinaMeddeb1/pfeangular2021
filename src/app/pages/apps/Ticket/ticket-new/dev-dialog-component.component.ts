import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {DialogData} from "./ticket-new.component";
import {User} from "../../../../core/models/auth.models";
import {RoleEnum} from "../../../../core/enumerations/role.enum";
import {UserProfileService} from "../../../../core/services/user.service";
import {TicketService} from "../../../../core/services/ticket.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-dev-dialog-component',
  templateUrl: './dev-dialog-component.component.html',
})
export class DevDialogComponentComponent  {

  constructor(
    public userProfileService: UserProfileService,
    public dialog: MatDialog,
    public router: Router,
    public ts: TicketService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.getUserByRole();
  }

  users: User[];



  getUserByRole() {
     this.ts.getUserResolve(this.data.ticket.id).subscribe(data => this.users = data);
    console.log(this.users);

  }

  submit(user: User) {
    this.ts.reafect(this.data.ticket.id, user);

  }
}
