import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TicketService} from '../../../../core/services/ticket.service';
import {ProjectService} from '../../../../core/services/project.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {Ticket} from '../../../../core/models/ticket';
import {UserProfileService} from '../../../../core/services/user.service';
import {observable, Observable} from 'rxjs';
import {User} from '../../../../core/models/auth.models';
import {RoleEnum} from '../../../../core/enumerations/role.enum';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Calendar} from "@fullcalendar/core/Calendar";
import {DevDialogComponentComponent} from "./dev-dialog-component.component";


export interface DialogData {
  ticket: Ticket;
}

@Component({
  selector: 'app-ticket-new',
  templateUrl: './ticket-new.component.html',
  styleUrls: ['./ticket-new.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }],
  encapsulation: ViewEncapsulation.None,
})
export class TicketNewComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  ticket: Ticket;
   idPr: number;
   conflict: boolean;
  isLinear = false;
  min: Date;
  users: User[];



  constructor(public ticketService: TicketService,
              private formBuilder: FormBuilder,
              public projectService: ProjectService,
              public userProfileService: UserProfileService,
              public actRoute: ActivatedRoute,
              public router: Router,
              public dialog: MatDialog
  ) {}


  ngOnInit() {
    this.ticket = new Ticket();
    this.min = new Date();







    // recuprerate id from path
    this.idPr = this.actRoute.snapshot.params.id;
    this.getUserByRole();

    this.firstFormGroup = this.formBuilder .group({title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      startDate: new FormControl(),
      endDate: new FormControl(),
      affectedTo: new FormControl(),
      estimation: new FormControl() ,

      children: this.formBuilder.array([])});



  }
  getUserByRole() {
   return this.userProfileService.getByRole(RoleEnum.DEVELOPER)
  .subscribe(users => {
      this.users = users;
    });
  }

  get children(): FormArray {
    return this.firstFormGroup.get('children') as FormArray;
  }
  newchild(): FormGroup {
    return this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  addChild() {
    this.children.push(this.newchild());
  }
  get diagnostic() {
    return JSON.stringify(this.firstFormGroup.value);
  }


  submit() {
    this.ticket = this.firstFormGroup.value;
    this.ticket.estimation = this.firstFormGroup.get('estimation').value + ':00';



    this.ticketService.addTicket(this.ticket, this.idPr).subscribe(data => { this.conflict = data;
        if (this.conflict) {

          this.openDialog(); } else {this.router.navigate(['project-detail', this.idPr]);  }

      },
      error => console.log(error)) ;



  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TicketNewDialogComponent, {
      data: {ticket: this.ticket},
      backdropClass: 'bdrop'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.ticket = result;
    });

  }

}

@Component({
  selector: 'app-ticket-new-dialog',
  templateUrl: 'dialog-elements-example-dialog.html',
})
export class TicketNewDialogComponent {
  constructor(

    public dialog: MatDialog,

    public router: Router,


    public dialogRef: MatDialogRef<TicketNewDialogComponent>,

    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }
  rediarect() {
    this.router.navigate(['apps-calendar']);
  }


  openDialog() {
    const dialogRef = this.dialog.open(DevDialogComponentComponent, {
      data: this.data
    });
    console.log(this.data);

    dialogRef.afterClosed().subscribe(result => {
    });

  }
}
