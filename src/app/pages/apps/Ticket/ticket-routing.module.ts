import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TicketDetailsComponent} from "./ticket-details/ticket-details.component";
import {TicketNewComponent} from "./ticket-new/ticket-new.component";


const routes: Routes = [  {
  path: 'ticket/details/:id',
  component: TicketDetailsComponent
}
  ,
  {
    path: ':id/ticket',
    component: TicketNewComponent
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketRoutingModule { }
