import {Component, Input, OnInit} from '@angular/core';
import {ProjectService} from "../../../../core/services/project.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Ticket} from "../../../../core/models/ticket";
import {TicketService} from "../../../../core/services/ticket.service";
import {UserProfileService} from "../../../../core/services/user.service";
import {User} from "../../../../core/models/auth.models";
import {AuthenticationService} from "../../../../core/services/auth.service";
import {GetImage} from "../../../../core/helpers/getImage";

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.scss']
})
export class TicketDetailsComponent implements OnInit {
  @Input() ticket: Ticket;
   user?: User;
  @Input() completed: boolean;
  @Input() subcompleted: boolean;

  current: User;
  subDone: boolean[];



  constructor(public router: Router, private userService: UserProfileService, private ticketService: TicketService,
              private auth: AuthenticationService) {
    this.subDone = new Array();
  }
  doesDataExist: boolean = false;



  ngOnInit() {
    this.user = this.ticket.affectedTo;
    this.current = this.auth.currentUser();


  }

  complete(c: boolean, id: number, ticket: Ticket) {
    console.log(this.completed);
    console.log(this.ticket.children);
    if (c === true) {


      this.ticketService.CompleteTicket(id, ticket).subscribe(
        value => {

          alert('' + value);

        },
        (er) => console.log(er.getMessages())
      );


    }


  }


}
