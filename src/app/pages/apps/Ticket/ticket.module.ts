import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketRoutingModule } from './ticket-routing.module';
import {TicketDetailsComponent} from "./ticket-details/ticket-details.component";
import {TicketListComponent} from "./ticket-list/ticket-list.component";
import {TicketNewComponent, TicketNewDialogComponent} from "./ticket-new/ticket-new.component";
import {MatStepperModule} from "@angular/material/stepper";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatListModule} from "@angular/material/list";
import {MatSelectModule} from "@angular/material/select";
import {NgSelectModule} from "@ng-select/ng-select";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import { DevDialogComponentComponent } from './ticket-new/dev-dialog-component.component';


@NgModule({
  declarations: [TicketDetailsComponent, TicketListComponent, TicketNewComponent, TicketNewDialogComponent, DevDialogComponentComponent],
  exports: [
    TicketDetailsComponent
  ],
  entryComponents: [TicketNewDialogComponent, DevDialogComponentComponent ],
  imports: [
    CommonModule,
    TicketRoutingModule,
    MatStepperModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatListModule,
    MatSelectModule,
    FormsModule,
    NgSelectModule,
    MatButtonModule,
    MatDialogModule
  ]
})
export class TicketModule { }
