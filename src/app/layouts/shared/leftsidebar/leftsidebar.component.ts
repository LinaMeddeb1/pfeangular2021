import {Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../../core/services/auth.service';
import { SIDEBAR_WIDTH_CONDENSED } from '../../layout.model';
import {ImageService} from "../../../core/services/image.service";
import {UploadedImage} from "../../../core/models/uploaded-image";
import {UserProfileService} from "../../../core/services/user.service";
import {User} from "../../../core/models/auth.models";
import {CookieService} from "../../../core/services/cookie.service";

@Component({
  selector: 'app-leftsidebar',
  templateUrl: './leftsidebar.component.html',
  styleUrls: ['./leftsidebar.component.scss'],

})
export class LeftsidebarComponent implements OnInit {

  @Input() sidebarType: string;

  user: User;
  currentFileUpload: UploadedImage;
  changeImage: boolean = false;
  uploading: boolean = false;
  imageToShow: any = null;
  profile: any;



  constructor(private router: Router, private authenticationService: AuthenticationService, private imageService: ImageService,
              private userService: UserProfileService, private  cookieService: CookieService) { }

  ngOnInit() {
    this.profile = JSON.parse(this.cookieService.getCookie('currentUser'));

    this.userService.fetchProfileThumbnail(this.profile.id.toString())
      .subscribe(image => this.createImage(image),
        err => this.handleImageRetrievalError(err));

  }
  private handleImageRetrievalError(err: Error) {
    console.error(err);
  }

  private createImage(image: Blob) {
    if (image && image.size > 0) {
      let reader = new FileReader();

      reader.addEventListener("load", () => {
        this.imageToShow = reader.result;
      }, false);

      reader.readAsDataURL(image);
    } else {
    }
  }



  /**
   * Is sidebar condensed?
   */
  isSidebarCondensed() {
    return this.sidebarType === SIDEBAR_WIDTH_CONDENSED;
  }


  /**
   * Logout the user
   */
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/account/login'], { queryParams: { returnUrl: '/' } });
  }
}
