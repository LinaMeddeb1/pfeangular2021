import { MenuItem } from './menu.model';

export const MENUDEV: MenuItem[] = [
  {
    label: 'Navigation',
    isTitle: true
  },
  {
    label: 'Dashboard',
    icon: 'home',
    link: '/',
    badge: {
      variant: 'success',
      text: '1',
    }
  },
  {
    label: 'Apps',
    isTitle: true
  },
  {
    label: 'Profile',
    icon: 'users',
    subItems: [
      {
        label: 'Profile',
        link: '/apps/users/profile'
      },

    ]
  },
  {
    label: 'My Projects',
    icon: 'briefcase',
    subItems: [
      {
        label: 'List',
        link: '/apps/project-list',
      }
    ]
  },
  {
    label: 'Calendar',
    icon: 'calendar',
    link: '/apps-calendar',
  },
  {
    label: 'Tasks',
    icon: 'bookmark',
    subItems: [
      {
        label: 'List',
        link: '/apps/task-list',
      },
      {
        label: 'Kanban Board',
        link: '/apps/task-board',
      },
    ]
  },
  {
    label: 'Email',
    icon: 'inbox',
    subItems: [
      {
        label: 'Inbox',
        link: 'inbox',
      },
  ]},
  {
    label: 'Quiz',
    icon: 'file-text',
    link: '/apps/theme',
  }];
